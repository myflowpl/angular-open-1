import { InjectionToken } from '@angular/core';

export const BASE_URL = new InjectionToken('BASE_URL');
export const IMAGES_BASE_URL = new InjectionToken('IMAGES_BASE_URL');
