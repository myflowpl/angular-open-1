import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, of, throwError, Subject, Observable, interval } from 'rxjs';
import { User } from './music/models/artist.model';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from './app-config';
import { switchMap, tap, startWith, merge, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';

const sessionDuration = 120;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private onRequest$ = new Subject();
  public refreshButtonClick$ = new Subject();

  private _idleTime$ = new BehaviorSubject(sessionDuration);

  private _user$ = new BehaviorSubject<null | User>(null);

  constructor(
    private dialog: MatDialog,
    private http: HttpClient,
    @Inject(BASE_URL) private baseUrl: string
  ) {

    const userFromStorage = localStorage.getItem('user');
    if (userFromStorage) {
      try {
        this._user$.next(JSON.parse(userFromStorage));
      } catch (error) {
        console.log('ERROR parsowania', error)
      }
    }
    console.log('USER FROM SESSION', userFromStorage);

    this._user$.subscribe(user => {
      console.log('SAVE SESSION', user);
      localStorage.setItem('user', JSON.stringify(user));
    });


    Observable.create().pipe(
      startWith(1),
      merge(this.onRequest$, this.refreshButtonClick$),
      switchMap(() => {
        return interval(1000);
      }),
      map((time: number) => {
        return sessionDuration - time;
      }),
      map((time: number) => {
        return time >= 0 ? time : 0;
      })
    ).subscribe(time => {
      if (!time && this._idleTime$.getValue() !== 0) {
        this.logout();
      }
      this._idleTime$.next(time);
    });
  }

  get idleTime$() {
    return this._idleTime$.asObservable();
  }

  get user$() {
    return this._user$.asObservable();
  }

  onRequest(req) {
    this.onRequest$.next();
  }

  login(username: string, password: string) {
    // TODO to jest kod mockowy do testowego api, na produkcji nie robimy tego GETem ;)
    return this.http.get<User[]>(this.baseUrl + '/users?name=' + username + '&password=' + password).pipe(
      switchMap((users) => {
        if (users.length) {
          return of(users[0]);
        } else {
          return throwError('not found');
        }
      }),
      tap(user => this._user$.next(user))
    );
  }

  logout() {
    this._user$.next(null);
  }

}
