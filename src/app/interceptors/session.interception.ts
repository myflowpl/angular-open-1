import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable,} from 'rxjs';
import { UserService } from '../user.service';

@Injectable()
export class SessionInterceptor implements HttpInterceptor {

  constructor(private userService: UserService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    this.userService.onRequest(req);

    return next.handle(req).pipe();
  }
}
