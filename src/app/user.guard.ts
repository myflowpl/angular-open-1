import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './user.service';
import { take, map } from 'rxjs/operators';
import { LoginService } from './shared/login.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log('GUARD ROLES', next.data);


    return this.loginService.loginDialog().pipe(
      take(1),
      map(user => {
        console.log('USER GUARD', user);

        if (next.data.roles && user && next.data.roles.indexOf(user.role) >= 0) {
          return true;
        } else {
          this.router.navigate(['/error/forbidden']);
          return false;
        }
      }),
      map(u => !!u)
    );
  }
}
