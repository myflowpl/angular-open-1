import { Component, OnInit } from '@angular/core';
import { Song } from '../models/artist.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-song-add',
  templateUrl: './song-add.component.html',
  styleUrls: ['./song-add.component.scss']
})
export class SongAddComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      console.log('RESOLVED DATA', data);
    });
  }

  onSave(data: Song) {
    console.log('SONG data', data);

  }

  onCancel() {
    console.log('cancel');
    this.router.navigateByUrl('/music/songs');
  }
}
