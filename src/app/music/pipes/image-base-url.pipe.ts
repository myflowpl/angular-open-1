import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IMAGES_BASE_URL } from 'src/app/app-config';

@Pipe({
  name: 'imageBaseUrl'
})
export class ImageBaseUrlPipe implements PipeTransform {

  constructor( @Inject(IMAGES_BASE_URL) private imageBaseUrl: string) {}
  transform(value: any, args?: any): any {
    return this.imageBaseUrl + value;
  }

}
