import { Component, OnInit, OnDestroy } from '@angular/core';
import { Song } from '../models/artist.model';
import { ActivatedRoute } from '@angular/router';
import { of, Observable, Subject } from 'rxjs';
import { takeUntil, switchMap, map } from 'rxjs/operators';
import { SongService } from '../song.service';

@Component({
  selector: 'app-song-view',
  templateUrl: './song-view.component.html',
  styleUrls: ['./song-view.component.scss']
})
export class SongViewComponent implements OnInit, OnDestroy {

  edit = false;

  destroy$ = new Subject();

  song$: Observable<Song>;

  constructor(
    private route: ActivatedRoute,
    private songService: SongService
  ) { }

  ngOnInit() {
    this.song$ = this.route.paramMap.pipe(

      takeUntil(this.destroy$),

      map(paramsMap => paramsMap.get('id')),

      switchMap((id) => {
        return this.songService.getSongById(id);
      }),

    );
  }

  onEdit() {
    this.edit = true;
  }

  onEditCancel() {
    this.edit = false;
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

}
