import { Component, OnInit } from '@angular/core';
import { MusicService } from '../music.service';
import { Artist } from '../models/artist.model';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public query = '';

  public artists$: Observable<Artist[]>;

  constructor(private musicService: MusicService) { }

  ngOnInit() {
    this.artists$ = this.musicService.getArtists().pipe(
      delay(500)
    );
  }

  search() {
    console.log('E', this.query);
    if (!this.query) { return; }

    this.artists$ = this.musicService.searchArtist(this.query).pipe(
      delay(500)
    );
  }

}
