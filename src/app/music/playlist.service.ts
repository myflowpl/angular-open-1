import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, of, interval } from 'rxjs';
import { Playlist } from './models/artist.model';
import { BASE_URL } from '../app-config';
import { tap, startWith, switchMap, map, merge, takeUntil, share, catchError, filter, scan, reduce, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  reload$ = new Subject();

  constructor(private http: HttpClient, @Inject(BASE_URL) private baseUrl: string) { }

  getPlaylists(): Observable<Playlist[]> {

    return Observable.create().pipe(
      startWith('START'),
      merge(this.reload$),
      switchMap((v) => {
        return this.http.get<Playlist[]>(this.baseUrl + '/playlists');
      })
    );
  }

  createPlaylist(data: Partial<Playlist>): Observable<Playlist> {
    return this.http.post<Playlist>(this.baseUrl + '/playlists', data).pipe(
      tap(playlist => {
        this.reload$.next(1);
      })
    );
  }

  updatePlaylist(id, data: Partial<Playlist>): Observable<Playlist> {
    return this.http.patch<Playlist>(this.baseUrl + '/playlists/' + id, data).pipe(
      tap(playlist => {
        this.reload$.next(1);
      })
    );
  }
}



// const myStream$ = Observable.create((observer) => {
//   let count = 0;
//   const sub = setInterval(() => {
//     // console.log('NEXT', count);
//     observer.next(count);
//     count++;
//     if (count > 3) {
//       observer.complete();
//     }
//   }, 100);

//   return () => {
//     // console.log('CANCELLLLL');
//     clearInterval(sub);
//   };
// });

// const stream2$ = myStream$.pipe(
//   tap(v => {
//     // console.log('v1', v);
//   }),
//   // map(v => v * v),
//   // filter(v => v < 3),
//   // reduce((count, v) => count + v, '--'),
//   switchMap(v => {
//     console.log('SWITCH MAP', v);
//     return interval(10).pipe(take(5));
//   }),
//   tap(v => {
//     console.log('v2', v);
//   })
//   // share()
// );

// setTimeout(() => {
//   stream2$.subscribe();
// }, 1000);

// const subscription = stream2$.subscribe(
  // (event) => console.log('SUCCESS', event),
  // (err) => console.log('ERR', err),
  // () => console.log('COMPLETE')
// );

// const subscription2 = myStream$.subscribe(
//   (event) => console.log('SUCCESS', event),
//   (err) => console.log('ERR', err),
//   () => console.log('COMPLETE')
// );


