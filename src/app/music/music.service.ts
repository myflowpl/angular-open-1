import { Injectable, InjectionToken, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { share, shareReplay } from 'rxjs/operators';
import { Artist } from './models/artist.model';
import { environment } from 'src/environments/environment';
import { BASE_URL } from '../app-config';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private http: HttpClient, @Inject(BASE_URL) private baseUrl: string) { }

  getArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.baseUrl + '/artists');
  }

  getArtist(id: string): Observable<Artist> {
    return this.http.get<Artist>(this.baseUrl + '/artists/' + id + '?_embed=songs');
  }

  searchArtist(query: string): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.baseUrl + '/artists?q=' + query);
  }

  updateArtist(id: string, data: Partial<Artist>): Observable<any> {
    return this.http.patch<any>(this.baseUrl + '/artists/' + id, data);
  }
}
