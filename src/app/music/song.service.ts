import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { BASE_URL } from '../app-config';
import { Song } from './models/artist.model';
import { startWith, merge, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  reload$ = new Subject();

  constructor(private http: HttpClient, @Inject(BASE_URL) private baseUrl: string) { }

  getSongs(): Observable<Song[]> {

    return Observable.create().pipe(
      startWith(1),
      merge(this.reload$),
      switchMap((v) => {
        return this.http.get<Song[]>(this.baseUrl + '/songs');
      })
    );
  }

  getSongById(id: string | number): Observable<Song> {
    return this.http.get<Song>(this.baseUrl + '/songs/' + id);
  }


}
