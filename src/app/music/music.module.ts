import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { SearchComponent } from './search/search.component';
import { SharedModule } from '../shared/shared.module';
import { ArtistProfileComponent } from './artist-profile/artist-profile.component';
import { ImageBaseUrlPipe } from './pipes/image-base-url.pipe';
import { ArtistComponent } from './artist/artist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArtistEditComponent } from './artist-edit/artist-edit.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistItemComponent } from './playlist-item/playlist-item.component';
import { PlaylistSongsComponent } from './playlist-songs/playlist-songs.component';
import { SongEditFormComponent } from './song-edit-form/song-edit-form.component';
import { SongsComponent } from './songs/songs.component';
import { SongViewComponent } from './song-view/song-view.component';
import { SongAddComponent } from './song-add/song-add.component';

@NgModule({
  imports: [
    CommonModule,
    MusicRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SearchComponent,
    ArtistProfileComponent,
    ImageBaseUrlPipe,
    ArtistComponent,
    ArtistEditComponent,
    PlaylistsComponent,
    PlaylistItemComponent,
    PlaylistSongsComponent,
    SongEditFormComponent,
    SongsComponent,
    SongViewComponent,
    SongAddComponent
  ]
})
export class MusicModule { }
