import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MusicService } from '../music.service';
import { switchMap, tap, takeUntil } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-artist-edit',
  templateUrl: './artist-edit.component.html',
  styleUrls: ['./artist-edit.component.scss']
})
export class ArtistEditComponent implements OnInit, OnDestroy {

  destroy$ = new Subject();
  artistForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private musicService: MusicService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      takeUntil(this.destroy$),
      switchMap((paramsMap) => {
        return this.musicService.getArtist(paramsMap.get('id'));
      }),
    ).subscribe((artist) => {
      this.artistForm.patchValue(artist);
    });

    this.artistForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      img: new FormControl(''),
    });
  }

  save() {
    const data = this.artistForm.getRawValue();
    console.log('DATA', data);
    this.musicService.updateArtist(data.id, data).subscribe(res => {
      console.log('RES', res);
      this.router.navigate(['music', 'search', 'artist', data.id]);
    });
  }

  ngOnDestroy() {
    this.destroy$.next(1);
  }

}
