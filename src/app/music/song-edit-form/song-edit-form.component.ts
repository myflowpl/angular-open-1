import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Song } from '../models/artist.model';
import { FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-song-edit-form',
  templateUrl: './song-edit-form.component.html',
  styleUrls: ['./song-edit-form.component.scss']
})
export class SongEditFormComponent implements OnInit {

  @Input() song: Song;
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();

  songForm = this.fb.group({
    title: ['', Validators.required],
    year: ['', Validators.required],
    favorite: [false, Validators.required],
    genders: this.fb.array([])
  });

  get genders() {
    return this.songForm.get('genders') as FormArray;
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    if (this.song) {
      if (this.song.genders) {
        this.song.genders.forEach(v => this.genders.push(this.fb.control('')));
      }
      this.songForm.patchValue(this.song);
    }
  }

  onSubmit() {
    console.log('SUBMIT', this.songForm.valid, this.songForm.value);

    this.save.next(this.songForm.value);

    // if (this.songForm.valid) {
    // this.save.next(this.songForm.value);
    // }
  }

  addGender() {
    this.genders.push(this.fb.control(''));
  }

  removeGender(index: number) {
    this.genders.removeAt(index);
  }
}
