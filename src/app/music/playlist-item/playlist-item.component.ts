import { Component, OnInit, Input } from '@angular/core';
import { Playlist } from '../models/artist.model';
import { FormControl } from '@angular/forms';
import { PlaylistService } from '../playlist.service';

@Component({
  selector: 'app-playlist-item',
  templateUrl: './playlist-item.component.html',
  styleUrls: ['./playlist-item.component.scss']
})
export class PlaylistItemComponent implements OnInit {

  @Input() playlist: Playlist;
  edit = false;
  name = new FormControl('');

  constructor(private playlistService: PlaylistService) { }

  ngOnInit() {
  }

  onEdit() {
    this.edit = true;
    this.name.setValue(this.playlist.name);
  }

  onCancel() {
    this.edit = false;
    this.name.setValue('');
  }

  onSave() {
    this.playlistService.updatePlaylist(this.playlist.id, {
      name: this.name.value
    }).subscribe();
  }
}
