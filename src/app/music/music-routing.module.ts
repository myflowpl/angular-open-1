import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { ArtistComponent } from './artist/artist.component';
import { ArtistEditComponent } from './artist-edit/artist-edit.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistSongsComponent } from './playlist-songs/playlist-songs.component';
import { SongEditFormComponent } from './song-edit-form/song-edit-form.component';
import { SongViewComponent } from './song-view/song-view.component';
import { SongAddComponent } from './song-add/song-add.component';
import { SongsComponent } from './songs/songs.component';
import { UserGuard } from '../user.guard';
import { of } from 'rxjs';
import { AuthorResolverService } from './sersolvers/author-resolver.service';

const routes: Routes = [{
  path: 'search',
  component: SearchComponent,
  children: [
    {
      path: 'artist/:id',
      component: ArtistComponent
    },
    {
      path: 'artist-edit/:id',
      component: ArtistEditComponent
    }
  ]
}, {
  path: 'playlists',
  component: PlaylistsComponent,
  children: [
    {
      path: 'songs/:id',
      component: PlaylistSongsComponent
    }
  ]
},
{
  path: 'songs',
  component: SongsComponent,
  children: [{
    path: 'add',
    component: SongAddComponent,
    canActivate: [UserGuard],
    data: {
      roles: ['admin']
    },
    resolve: {
      artists: AuthorResolverService
    }
  }, {
    path: ':id',
    component: SongViewComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
