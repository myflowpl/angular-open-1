import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PlaylistService } from '../playlist.service';
import { Playlist } from '../models/artist.model';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit, OnDestroy {

  name = new FormControl('');
  destroy$ = new Subject();
  playlists$: Observable<Playlist[]>;

  constructor(public playlistService: PlaylistService) { }

  ngOnInit() {
    this.playlists$ = this.playlistService.getPlaylists();
  }

  add() {
    this.playlistService.createPlaylist({ name: this.name.value }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(playlist => {
      this.name.setValue('');
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
