import { Injectable } from '@angular/core';
import { Artist } from '../models/artist.model';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { MusicService } from '../music.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorResolverService implements Resolve<Artist[]> {

  constructor(private artist: MusicService) { }

  resolve(): Observable<Artist[]> {
    return this.artist.getArtists();
  }
}
