import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BASE_URL, IMAGES_BASE_URL } from './app-config';
import { environment } from 'src/environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SessionInterceptor } from './interceptors/session.interception';
import { ForbiddenPageComponent } from './forbidden-page/forbidden-page.component';
import { LoginDirective } from './login.directive';

@NgModule({
  declarations: [
    AppComponent,
    ForbiddenPageComponent,
    LoginDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [

    { provide: HTTP_INTERCEPTORS, useClass: SessionInterceptor, multi: true },

    {
      provide: BASE_URL,
      useValue: environment.baseUrl
    },
    {
      provide: IMAGES_BASE_URL,
      useValue: environment.imagesBaseUrl
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
