import { Component } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { MatDialog } from '@angular/material';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserService } from './user.service';
import { LoginService } from './shared/login.service';

interface Item {
  id: number;
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'MapTodo';
  isLoading = true;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    public userService: UserService,
    public loginService: LoginService
  ) {
    router.events.subscribe((routerEvent: Event) => {
      this.checkEvent(routerEvent);
    });
  }

  checkEvent(routerEvent: Event): void {
    if (routerEvent instanceof NavigationStart) {
      this.isLoading = true;
    } else if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.isLoading = false;
    }
  }
}
