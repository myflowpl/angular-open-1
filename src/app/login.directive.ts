import { Directive, HostListener } from '@angular/core';
import { LoginService } from './shared/login.service';

@Directive({
  selector: '[appLogin]'
})
export class LoginDirective {

  constructor(private loginService: LoginService) { }

  @HostListener('click')
  onclick() {
    this.loginService.loginDialog().subscribe();
  }
}
