import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapComponent } from './shared/map/map.component';
import { SharedModule } from './shared/shared.module';
import { ForbiddenPageComponent } from './forbidden-page/forbidden-page.component';

const routes: Routes = [
  {
    path: '',
    component: MapComponent
  },
  {
    path: 'error/:type',
    component: ForbiddenPageComponent
  },
  {
    path: 'music',
    loadChildren: './music/music.module#MusicModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), SharedModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
