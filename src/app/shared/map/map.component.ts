import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, OnChanges, SimpleChange, Input } from '@angular/core';
import { ListComponent } from '../list/list.component';
import { Artist } from 'src/app/music/models/artist.model';
import { ReplaySubject } from 'rxjs';

L.Icon.Default.imagePath = '/assets/leaflet/images/';

export interface Coords {
  lat: number;
  lng: number;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {

  @ViewChild('mapContainer') mapContainer: ElementRef;

  @Output() mapClick = new EventEmitter<Coords>();

  @Input() artist: Artist;
  private artist$ = new ReplaySubject<Artist>(1);

  constructor() { }

  ngOnInit() {

    const map = L.map(this.mapContainer.nativeElement).setView([51.505, -0.09], 13);

    L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png`', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    map.on('click', (e) => {
      this.mapClick.next(e.latlng);
    });

    this.artist$.subscribe((artist: Artist) => {

      L.marker(artist.location)
         .addTo(map)
        .bindPopup(artist.name)
        .openPopup();
      map.panTo(artist.location);

    });


  }

  ngOnChanges(changes: { [key: string]: SimpleChange }) {
    if (changes.hasOwnProperty('artist')) {
      this.artist$.next(changes['artist'].currentValue);
    }
  }
}
