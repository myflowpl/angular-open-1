import { Directive, HostBinding, HostListener, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {


  @HostBinding('style.text-decoration')
  decoration = '';

  constructor(private el: ElementRef, private renderer: Renderer) {  }

  @HostListener('mouseover')
  onmouseover() {
    console.log('over');
    this.decoration = 'underline';

    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', 'yellow');
  }

  @HostListener('mouseout')
  onmouseout() {
    console.log('out');
    this.decoration = '';
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', '');
  }

}
