import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { MapComponent } from './map/map.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatButtonModule, MatInputModule } from '@angular/material';
import { UserLoginComponent } from '../user-login/user-login.component';
import { HighlightDirective } from './directives/highlight.directive';
import { IsAdminDirective } from './directives/is-admin.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
  ],
  entryComponents: [
    UserLoginComponent
  ],
  declarations: [
    ListComponent,
    MapComponent,
    UserLoginComponent,
    HighlightDirective,
    IsAdminDirective],
  exports: [
    ListComponent,
    MapComponent,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    UserLoginComponent,
    HighlightDirective,
    IsAdminDirective
  ]
})
export class SharedModule { }
